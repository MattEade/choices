namespace Patterns
{
	public class Pattern
	{
		public enum AnswerType
		{
			none,
			right,
			wrong,
			random
		}

		private string patternName;
		private PatternDefinition patternDefinition;

public Pattern(PatternDefinition patternDefinition)
		{
			this.patternDefinition = patternDefinition;
			patternName = patternDefinition.PatternName;
		}
		public AnswerDefinition GetAnwser(AnswerType answerType)
		{
			return patternDefinition.GetAnwser(answerType);
		}	
	}
}
