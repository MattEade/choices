using UnityEngine;

namespace Patterns
{
	public class AnswerDefinition
	{
		private bool isCorrect;
		private string answerText;

		private Texture answerTexture;
		public bool IsCorrect { get => isCorrect; set => isCorrect = value; }
		public string AnswerText { get => answerText; set => answerText = value; }
		public Texture AnswerTexture { get => answerTexture; set => answerTexture = value; }
	}
}
