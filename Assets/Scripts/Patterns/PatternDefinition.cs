using UnityEngine;

namespace Patterns
{
	public abstract class PatternDefinition : ScriptableObject
	{
		public string PatternName;

		public abstract AnswerDefinition GetAnwser(Pattern.AnswerType answerType);
	}
}
