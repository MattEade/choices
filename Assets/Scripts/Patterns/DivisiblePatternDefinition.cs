using System;
using UnityEngine;

namespace Patterns
{
	[CreateAssetMenu(fileName = "DivisiblePatternDefinition", menuName = "Choices/PatternDefinitions/DivisiblePatternDefinition", order = 0)]
	public class DivisiblePatternDefinition : PatternDefinition
	{
		[Tooltip("If -1 will choose a random numer")]
		public int DivisibleBy = -1;
		public int MaxAnswer;
		[Tooltip("Tick for non divisible to be correct")]
		public bool inverse;
		public override AnswerDefinition GetAnwser(Pattern.AnswerType answerType)
		{
			if (DivisibleBy == -1)
			{
				DivisibleBy = UnityEngine.Random.Range(2, (int) (MaxAnswer / 10.0f));
			}

			string answerString = null;
			var isCorrect = false;

			switch (answerType)
			{
				case Pattern.AnswerType.right:
					answerString = inverse?GetNonDivisibleNumber() : GetDivisibleNumber();
					isCorrect = true;
					break;

				case Pattern.AnswerType.wrong:
					answerString = inverse? GetDivisibleNumber() : GetNonDivisibleNumber();
					break;

				case Pattern.AnswerType.random:
					var choiceNum = UnityEngine.Random.value;
					answerString = choiceNum > 0.5f ? GetDivisibleNumber() : GetNonDivisibleNumber();
					isCorrect = choiceNum > 0.5f ? true : false;
					isCorrect = inverse? !isCorrect: isCorrect;
					break;
			}

			var answer = new AnswerDefinition { AnswerText = answerString, IsCorrect = isCorrect };
			return answer;
		}

		private string GetDivisibleNumber()
		{
			var answerString = ((int) ((int) (MaxAnswer / (DivisibleBy * 1f)) * UnityEngine.Random.value) * DivisibleBy).ToString();
			return answerString;
		}

		private string GetNonDivisibleNumber()
		{
			var answerString = (((int) ((int) (MaxAnswer / (DivisibleBy * 1f)) * UnityEngine.Random.value) * DivisibleBy) - (UnityEngine.Random.Range(1, DivisibleBy - 1))).ToString();
			return answerString;
		}
	}
}
