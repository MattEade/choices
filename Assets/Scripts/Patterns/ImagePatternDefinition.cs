using System;
using DUCK.Utils;
using UnityEngine;
namespace Patterns
{
	[CreateAssetMenu(fileName = "TexturePatternDefinition", menuName = "Choices/PatternDefinitions/TexturePatternDefinition", order = 0)]
	public class TexturePatternDefinition : PatternDefinition
	{
		[Tooltip("Tick for odd one out to be correct")]
		public bool inverse;
		public Texture[] Matching;
		public Texture[] NonMatching;

		public override AnswerDefinition GetAnwser(Pattern.AnswerType answerType)
		{
			var isCorrect = false;
			Texture answerTexture = null;

			switch (answerType)
			{
				case Pattern.AnswerType.right:
					answerTexture = inverse? GetFromNonMatching() : GetFromMatching();
					isCorrect = true;
					break;

				case Pattern.AnswerType.wrong:
					answerTexture = inverse? GetFromMatching() : GetFromNonMatching();
					break;

				case Pattern.AnswerType.random:
					var choiceNum = UnityEngine.Random.value;
					answerTexture = choiceNum > 0.5f ? GetFromMatching() : GetFromNonMatching();
					isCorrect = choiceNum > 0.5f ? true : false;
					isCorrect = inverse?!isCorrect : isCorrect;
					break;
			}

			var answer = new AnswerDefinition { AnswerTexture = answerTexture, IsCorrect = isCorrect };
			return answer;
		}

		private Texture GetFromMatching()
		{
			return Matching.Random();
		}

		private Texture GetFromNonMatching()
		{
			return NonMatching.Random();
		}
	}
}
