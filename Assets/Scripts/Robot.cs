﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.GoBot
{
	public enum RobotOrientation
	{
		vertical,
		horizontal
	}

	public class Robot : MonoBehaviour
	{
		[SerializeField]
		private int id = 1;

		[SerializeField]
		private float speed = 0.1f;

		[SerializeField]
		private GameObject explosion;

		[SerializeField]
		private string keyName;

		[SerializeField]
		private GameObject trackLeft;

		[SerializeField]
		private GameObject trackRight;

		[SerializeField]
		private List<GameObject> leftCogs;

		[SerializeField]
		private List<GameObject> rightCogs;

		[SerializeField]
		private float wheelSpeedModifier = 0.1f;

		[SerializeField]
		private float trackSpeedModifier = 0.1f;

		[SerializeField]
		private float trackSpeedRotaionModifier = 0.1f;

		[SerializeField]
		private float turnDuration = 2;

		[SerializeField]
		private AudioSource audioSource;

		[SerializeField]
		private AudioClip forwardSound;

		[SerializeField]
		private AudioClip turnSound;



		private bool IsMoving
		{
			set
			{
				isMoving = value;

				if (isMoving)
				{
					robotAnimator.SetBool("Walk", true);
				}
				else
				{
					robotAnimator.SetBool("Idle", true);
				}
			}

			get
			{
				return isMoving;
			}
		}

		public RobotOrientation Orientation
		{
			get
			{
				return orientation;
			}

			private set
			{
				orientation = value;
			}
		}

		private Animator robotAnimator;
		private Vector3 robotRotaion = Vector3.zero;
		private float deathRotation = 0;
		private bool isTurning;
		private Vector3 startRotation;
		private float turnTime;
		private bool isMoving = true;
		private RobotOrientation orientation = RobotOrientation.horizontal;
		private Material trackMaterialLeft;
		private Material trackMaterialRight;

		private void Awake()
		{
			var leftTrackRenderer = trackLeft.GetComponent<Renderer>();
			leftTrackRenderer.material.mainTexture = Instantiate(leftTrackRenderer.material.mainTexture) as Texture;
			trackMaterialLeft = leftTrackRenderer.material;

			var rightTrackRenderer = trackRight.GetComponent<Renderer>();
			rightTrackRenderer.material.mainTexture = Instantiate(leftTrackRenderer.material.mainTexture) as Texture;
			trackMaterialRight = rightTrackRenderer.material;
		}

		private void Start()
		{
			robotAnimator = GetComponent<Animator>();
			Orientation = (Math.Abs(transform.rotation.y) > 0) ? RobotOrientation.vertical : RobotOrientation.horizontal;
			var rotation = transform.eulerAngles;
			rotation.y += -90;
			transform.eulerAngles = rotation;
			audioSource.clip = forwardSound;
			audioSource.Play();
		}

		private void Update()
		{
			var trackSpeed = speed * trackSpeedModifier;

			if (isTurning)
			{
				var rotation = transform.eulerAngles;
				float rotationY = 0;
				turnTime += Time.deltaTime;

				if (turnTime >= turnDuration)
				{
					isTurning = false;
					audioSource.clip = forwardSound;
					rotationY = startRotation.y + 180;
				}
				else
				{
					rotationY = Mathf.LerpAngle(startRotation.y, startRotation.y + 180, turnTime / turnDuration);
				}

				rotation.y = rotationY;
				transform.eulerAngles = rotation;
				UpdateTrack(trackMaterialLeft, trackSpeed);
				UpdateTrack(trackMaterialRight, -trackSpeed);
				UpdateCogs(leftCogs, speed);
				UpdateCogs(rightCogs, -speed);
			}
			else
			{
				if (IsMoving)
				{
					gameObject.transform.Translate(speed * Time.deltaTime, 0, 0);
					UpdateTrack(trackMaterialLeft, trackSpeed);
					UpdateTrack(trackMaterialRight, trackSpeed);
					UpdateCogs(leftCogs, speed);
					UpdateCogs(rightCogs, speed);

					if (audioSource.isPlaying == false)
					{
						audioSource.Play();
					}
				}
				else
				{
					if (audioSource.isPlaying == true)
					{
						audioSource.Stop();
					}
				}
			}




		}

		private void UpdateTrack(Material trackMaterial, float trackSpeed)
		{
			var textureOffset = trackMaterial.mainTextureOffset;
			textureOffset.y += trackSpeed;
			textureOffset.y = textureOffset.y % 1;
			trackMaterial.mainTextureOffset = textureOffset;
		}

		private void UpdateCogs(List<GameObject> cogs, float cogSpeed)
		{
			foreach (var cog in cogs)
			{
				var rotation = new Vector3(cogSpeed * wheelSpeedModifier, 0, 0);

				cog.transform.Rotate(rotation);
			}
		}

		private void OnTriggerEnter(Collider other)
		{
			if (other.gameObject.tag.Equals("Wall") || other.gameObject.tag.Equals("Exit") || other.gameObject.tag.Equals("GuardTurn"))
			{
				Turn();
			}

			if (other.gameObject.tag.Equals("Guard"))
			{
				var otherGuard = other.gameObject.GetComponent<Robot>();
				var otherPos = other.gameObject.transform.position;
				var pos = gameObject.transform.position;
				var rotY = gameObject.transform.rotation.y;

				if (otherGuard.Orientation == Orientation)
				{
					Turn();
				}
				else
				{
					if (Orientation == RobotOrientation.horizontal)
					{


						if (Mathf.Abs(pos.z - otherPos.z) < 2.1f)
						{
							Turn();
						}
					}
					else
					{
						if (Mathf.Abs(pos.x - otherPos.x) < 2.1f)
						{
							Turn();
						}
					}
				}
			}

			if (other.gameObject.tag.Equals("Kill"))
			{
				IsMoving = false;
			}
		}

		private void Turn()
		{
			if (isTurning == false)
			{
				isTurning = true;
				startRotation = transform.eulerAngles;
				turnTime = 0;
				audioSource.clip = turnSound;
				audioSource.Play();
			}
		}

		private void OnTriggerExit(Collider other)
		{
			if (other.gameObject.tag.Equals("Kill"))
			{
				if (!IsMoving)
				{
					IsMoving = true;
				};
			}
		}

		private void OnTriggerStay(Collider other)
		{
			if (other.gameObject.tag.Equals("Wall") || other.gameObject.tag.Equals("Exit") || other.gameObject.tag.Equals("GuardTurn"))
			{
				Turn();
			}
		}
	}
}

