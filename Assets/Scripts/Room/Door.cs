using Duck.HierarchyBehaviour;
using TMPro;
using UnityEngine;

namespace Room
{
	public class Door : MonoBehaviour, IHierarchyBehaviour
	{
		public string Id;
		
		[SerializeField]
		private Animator doorAnimator;

		[SerializeField]
		private TextMeshProUGUI doorText;
		private bool isExit;

		public bool IsExit { get => isExit; set => isExit = value; }
		public Animator DoorAnimator { get => doorAnimator; set => doorAnimator = value; }

		public void Initialize()
		{
			gameObject.SetActive(true);
		}

		public void SetDoorText(string answerText)
		{
			doorText.text = answerText;
		}

		public void OpenDoor()
		{
			//gameObject.SetActive(false);
			//var doorAnimator = gameObject.GetComponentInChildren<Animator>();
			doorAnimator.SetBool("MakeDoorOpen", true);
		}
	}
}
