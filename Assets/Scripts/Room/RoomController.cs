﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using Duck.HierarchyBehaviour;
using DUCK.Utils;
using Room;
using UnityEngine;

public class RoomController : MonoBehaviour, IHierarchyBehaviour
{
	[SerializeField]
	private List<Door> doors;

	[SerializeField]
	private GameObject correctTunnel;

	private RoomDefinition roomDefinition;

	private List<float> targetPositions;

	private float correctTargetX;

	private Door correctDoor;

	private bool isComplete;

	private float roomZ;

	public float CorrectTargetX { get => correctTargetX; set => correctTargetX = value; }
	public List<float> TargetPositions { get => targetPositions; set => targetPositions = value; }
	public bool IsComplete { get => isComplete; set => isComplete = value; }
	public float RoomZ { get => roomZ; set => roomZ = value; }

	public void Initialize()
	{

	}

	public void InitialiseRoom(RoomDefinition roomDefinition)
	{
		this.roomDefinition = roomDefinition;
		var answers = roomDefinition.AnswerDefinitions.Shuffle();
		TargetPositions = new List<float>();

		for (int i = 0; i < doors.Count; i++)
		{
			var door = doors[i];
			door.Initialize();
			door.Id = roomDefinition.RoomId + "_D_" + i;
			var answerDefinition = answers[i];
			door.SetDoorText(answerDefinition.AnswerText);
			var doorPosX = door.transform.position.x;
			TargetPositions.Add(doorPosX);

			if (answerDefinition.IsCorrect)
			{
				var correctPosition = correctTunnel.transform.position;
				CorrectTargetX = doorPosX;
				correctDoor = door;
				correctPosition.x = CorrectTargetX;
				correctTunnel.transform.position = correctPosition;
			}
		}

		isComplete = false;
		TargetPositions.Sort();
		TargetPositions.Reverse();
	}

	public void OpenDoor()
	{
		var animator = correctDoor.GetComponentInChildren<Animator>();
		UnityEngine.Debug.Log("animator.gameObject.activeSelf: " + animator.gameObject.activeSelf);
		UnityEngine.Debug.Log("DoorId: " + animator.gameObject.transform.parent.gameObject.GetComponentInChildren<Door>().Id);
		UnityEngine.Debug.Log("animator.runtimeAnimatorController: " + animator.runtimeAnimatorController);
		UnityEngine.Debug.Log("animator.runtimeAnimatorController.GetInstanceID(): " + animator.runtimeAnimatorController.GetInstanceID());

		animator.SetTrigger("MakeDoorOpen");

		//correctDoor.OpenDoor();
	}
}
