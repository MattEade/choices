using System;
using UnityEngine;

namespace Room
{
	[CreateAssetMenu(fileName = "RoomCreationData", menuName = "choices/RoomCreationData", order = 0)]
	public class RoomCreationData : ScriptableObject
	{

		public GameObject RoomPrefab;
		public RoomController RoomController;
		public int NumberOfDoors;

	}
}
