using System.Collections;
using System.Collections.Generic;
using DUCK.Utils;
using Patterns;
using UnityEngine;
namespace Room
{
	public class RoomDefinition
	{
		private List<AnswerDefinition> answerDefinitions;
		public List<AnswerDefinition> AnswerDefinitions { get => answerDefinitions; set => answerDefinitions = value; }
		public GameObject RoomPrefab { get => roomPrefab; set => roomPrefab = value; }
		public RoomController RoomController { get => roomController; set => roomController = value; }
		public string RoomId { get => roomId; set => roomId = value; }

		private GameObject roomPrefab;
		private RoomController roomController;
		private string roomId;

		public RoomDefinition(string roomId)
		{
			this.RoomId = roomId;
			answerDefinitions = new List<AnswerDefinition>();
		}
	}
}
