using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Schema;
using Duck.HierarchyBehaviour;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.UI.CoroutineTween;

namespace Game
{
	public class MainController : MonoBehaviour, IHierarchyBehaviour
	{
		private enum characterStates
		{
			None,
			Running,
			Opening,
			Crashing
		}

		[SerializeField]
		private Animator characterAnimator;

		[SerializeField]
		private BuildingController buildingController;

		[SerializeField]
		private float characterSpeedZ;

		[SerializeField]
		private float doorZOffset;

		[SerializeField]
		private float doorLeeway = 0.5f;
		private float targetX;

		[SerializeField]
		private Button LeftArrowButton;

		[SerializeField]
		private Button RightArrowButton;

		private float targetZ;

		private characterStates characterState;

		private RoomController currentRoomController;

		private RoomController previousRoomController;

		private GameObject character;

		public int charXposIndex;

		private bool isNewRoom;

		private void Awake()
		{
			Initialize();
		}
		private void Start()
		{

		}

		public void Initialize()
		{
			character = characterAnimator.gameObject;
			LeftArrowButton.onClick.AddListener(MoveLeft);
			RightArrowButton.onClick.AddListener(MoveRight);
			AddRoom();

			Run();
		}

		private void MoveLeft()
		{
			charXposIndex += 1;
			isNewRoom = false;
		}

		private void MoveRight()
		{
			charXposIndex -= 1;
			isNewRoom = false;
		}

		private void AddRoom()
		{
			previousRoomController = currentRoomController;
			currentRoomController = null;
			currentRoomController = buildingController.NextRoom();
			targetX = currentRoomController.CorrectTargetX;
			targetZ = doorZOffset + currentRoomController.RoomZ;

			var characterPosX = character.transform.position.x;
			var lastDif = 100000f;

			for (int i = 0; i < currentRoomController.TargetPositions.Count; i++)
			{
				var dif = Mathf.Abs(characterPosX - currentRoomController.TargetPositions[i]);
				if (dif < lastDif)
				{
					charXposIndex = i;
				}

				lastDif = dif;
			}

			isNewRoom = true;
		}

		private void Run()
		{
			characterState = characterStates.Running;
			characterAnimator.SetTrigger("Run");
		}

		private void Update()
		{
			var characterPos = character.transform.position;

			if (!isNewRoom)
			{
				if (charXposIndex < 0)
				{
					charXposIndex = 0;
				}

				if (charXposIndex > currentRoomController.TargetPositions.Count - 1)
				{
					charXposIndex = currentRoomController.TargetPositions.Count - 1;
				}

				characterPos.x = Mathf.Lerp(characterPos.x, currentRoomController.TargetPositions[charXposIndex], 0.05f);
			}

			switch (characterState)
			{
				case characterStates.Running:
					var newPosZ = characterPos.z + characterSpeedZ * Time.deltaTime;

					if (newPosZ > targetZ && !currentRoomController.IsComplete)
					{
						currentRoomController.IsComplete = true;
						newPosZ = targetZ;

						//if (characterPos.x < targetX + doorLeeway && characterPos.x > targetX - doorLeeway)
						//	{
						characterState = characterStates.Opening;
						characterAnimator.SetTrigger("PushDoor");
						currentRoomController.OpenDoor();
						AddRoom();
						/* }
						else
						{
							characterState = characterStates.Crashing;
							characterAnimator.SetTrigger("HitDoor");
						}*/
					}

					characterPos.z = newPosZ;
					character.transform.position = characterPos;
					break;

				case characterStates.Opening:

					newPosZ = characterPos.z + characterSpeedZ * Time.deltaTime;

					if (newPosZ > targetZ - 10)
					{
						Run();

						if (previousRoomController != null)
						{
							Destroy(previousRoomController.gameObject);
						}

						isNewRoom = false;
					}

					characterPos.z = newPosZ;
					character.transform.position = characterPos;
					break;
			}
		}

	}
}
