﻿using Duck.HierarchyBehaviour;
using DUCK.Utils;
using Patterns;
using Room;
using UnityEngine;

namespace Game
{
	public class BuildingController : MonoBehaviour
	{
		[SerializeField]
		private PatternDefinition[] patternDefinitions;

		[SerializeField]
		private RoomCreationData[] roomCreationDatas;

		[SerializeField]
		private float roomSpacing;

		[SerializeField]
		private int maxSequenceRooms;

		[SerializeField]
		private int minSequenceRooms;

		private RoomSequence roomSequence;

		private RoomController currentRoom;

		private RoomController nextRoom;

		private int roomsSoFar;

		private int roomSequenceCount;

		public RoomController NextRoom()
		{
			if (roomSequence == null)
			{
				NewSequence();
			}

			var nextRoomDefinition = roomSequence.Next();

			if (nextRoomDefinition == null)
			{
				NewSequence();
				nextRoomDefinition = roomSequence.Next();
			}

			return BuildRoom(nextRoomDefinition);
		}

		private RoomController BuildRoom(RoomDefinition roomDefinition)
		{
			var spawnZ = roomsSoFar * roomSpacing;
			var roomPosition = new Vector3(0, 0, spawnZ);
			var room = gameObject.CreateChild(roomDefinition.RoomPrefab);
			room.transform.position = roomPosition;
			var roomController = room.GetComponent<RoomController>();
			roomController.RoomZ = spawnZ;
			roomController.InitialiseRoom(roomDefinition);
			roomsSoFar++;
			return roomController;
		}

		private void NewSequence()
		{
			var pattern = new Pattern(patternDefinitions.Random());
			var roomCount = Random.Range(minSequenceRooms, maxSequenceRooms);
			roomSequence = new RoomSequence();

			for (int i = 0; i < roomCount; i++)
			{
				var id = "S_" + roomSequenceCount + "_R_" + i;
				var roomDefinition = CreateRoomDefinition(pattern, id);
				roomSequence.RoomDefinitions.Add(roomDefinition);
			}

			roomSequenceCount++;
		}
		private RoomDefinition CreateRoomDefinition(Pattern pattern, string roomId)
		{
			var roomCreationData = roomCreationDatas.Random();
			var roomDefinition = new RoomDefinition(roomId);

			for (int i = 0; i < roomCreationData.NumberOfDoors; i++)
			{
				var answerType = i == 0 ? Pattern.AnswerType.right : Pattern.AnswerType.wrong;
				var answerDefinition = pattern.GetAnwser(answerType);
				roomDefinition.AnswerDefinitions.Add(answerDefinition);
			}

			roomDefinition.RoomPrefab = roomCreationData.RoomPrefab;
			roomDefinition.RoomController = roomCreationData.RoomController;
			return roomDefinition;
		}
	}
}
