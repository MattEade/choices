using System.Collections.Generic;
using Patterns;
using Room;

namespace Game
{
	public class RoomSequence
	{
		private Pattern pattern;
		private List<RoomDefinition> roomDefinitions;

		public List<RoomDefinition> RoomDefinitions { get => roomDefinitions; set => roomDefinitions = value; }

		private int progressIndex;
		public RoomSequence()
		{
			roomDefinitions = new List<RoomDefinition>();
			progressIndex = 0;
		}

		public RoomDefinition Next()
		{
			if (progressIndex > roomDefinitions.Count - 1)
			{
				return null;
			}

			var roomDefinition = roomDefinitions[progressIndex];
			progressIndex++;
			
			return roomDefinition;
		}

	}
}
